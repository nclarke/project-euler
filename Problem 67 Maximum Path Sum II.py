import os

os.chdir('C:\Python32\Examples\Project Euler')

# open file
f = open ("Problem 67.txt","r")

#Read in the text file by line
f = [x.split() for x in f.read().splitlines()]

#Convert numbers in triangle to integers (from default strings)
for i in range(len(f)):
	for j in range(len(f[i])):
		f[i][j] = int(f[i][j])

#Now the idea here is instead of working our way down the tree we will work our way up from the bottom.
#We can evaluate the maximum path sum of all of the sub trees by starting at the bottom with a node and it's two children and computing it's
#maximum path sum as it's own value and the max of it's two children.
#The key here is that once we know the maximum path sums from each of the nodes in the ith layer we do not need to consider the ith + 1 layer at all.
#Thus iterate this process up the tree until we reach the maximum path sum of the whole tree at the top.
for i in range(len(f)-2,-1,-1):
	for j in range(len(f[i])):
		f[i][j] = f[i][j] + max(f[i+1][j],f[i+1][j+1])

print(f[0][0])