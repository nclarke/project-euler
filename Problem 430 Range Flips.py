#Problem 430

#Work out prob of a place not being flipped (both A and B are both left or both are right)
#work out prob of it still being white after m turns

##Probability of an even number of flips after m turns is 0.5*(1+(q-p)**m)
#http://mathforum.org/library/drmath/view/56700.html
#This is the prob of staying white

def ExpectedWhites(n,m):
    import time
    t = time.time()
    count = 0
    for i in range(1,int(n/2 + 1)):
        #This will only work for even n (can be easily modified) but cuts computation in half
        p = ((i-1)/n)**2 + ((n-i)/n)**2
        q = 1-p
        ProbOfWhite = 0.5*(1+(q-p)**m)
        #Prob of an even number of flips
        count += ProbOfWhite
    return(count*2, 'Time:', time.time() -t)

    
