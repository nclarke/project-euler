#Problem 69:

def isPrime(n):
    import math
    assert n>0
    if n==1:
        return False
    else:
        if n<4:
            return True
        else:
            if  n%2==0:
                return False
            else:
                if n<9:
                    return True
                else:
                    if n%3==0:
                        return False
                    else:
                        r=math.floor(math.sqrt(n))  
                        f=5
                        while f<=r:
                            if n%f==0:
                                return False
                                break
                            if n%(f+2)==0:
                                return False
                                break
                            f=f+6
                        return True
                                 
def PrimeFactors(i):
   Factors = list()
   import math
   j = i
   while not isPrime(j):
       for a in range(2, math.floor(math.sqrt(j) + 1)):
           if j%a == 0:
               Factors += (a,)
               j = j/a
               break
   Factors += (j,)
   return list(set(Factors))
   
def totient(n):
   product = 1
   for p in PrimeFactors(n):
       product = product*(1-1/p)
   return n*product

max = 1
value = 1
for i in range(2, 1000001):
   if i/totient(i) > max:
       max = i/totient(i)
       value = i


print(value)
print(max)

#Euler's totient function is very useful here.
#I think my PrimesFactors function is reasonably efficient.