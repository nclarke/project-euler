# So first I noted that the maximun square digit sum in the range is
# 567 (9 * 9**2), so over the 10 million numbers that's a lot of duplication.

# The basic idea then is to determine which numbers in 1-567 end at 89 and
# then compare the first square digit sum of every subsequent number to this list.

list_89 = list()


def digit_square_sum(n):
	total = 0
	for i in str(n):
		total += int(i)**2
	return total

for i in range(1, 567 + 1):
	chain = (i,)
	while chain[-1] != 1 and chain[-1] != 89:
		chain += (digit_square_sum(chain[-1]),)

	if chain[-1] == 89:
		list_89 += (i,)

count = 0

for i in range(1, 10**7 + 1):
	if digit_square_sum(i) in list_89:
		count += 1

print(count)
