import os

os.chdir('C:\Python32\Examples\Project Euler')

# open file
f = open ("Problem 81.txt","r")

#Read in the text file by line
f = [x.split(",") for x in f.read().splitlines()]

#Convert numbers in matrix to integers (from default strings)
for i in range(len(f)):
	for j in range(len(f[i])):
		f[i][j] = int(f[i][j])

#Along the bottom row we can only move right
for i in range(len(f[-1])-2,-1,-1):
	f[-1][i] = f[-1][i] + f[-1][i+1]

#Along the right column we can only move down
for i in range(len(f)-2,-1,-1):
	f[i][-1] = f[i][-1] + f[i+1][-1]

#Same idea as problem 67 in essence, start from bottom right corner, set each element equal to the min of the node below it and the node
#to the right and work backwards up the matrix
for i in range(len(f)-2,-1,-1):
	for j in range(len(f[i])-2,-1,-1):
		f[i][j] = f[i][j] + min(f[i+1][j],f[i][j+1])

print(f[0][0])
