#Problem 70:

def isPrime(n):
    import math
    assert n>0
    if n==1:
        return False
    else:
        if n<4:
            return True
        else:
            if  n%2==0:
                return False
            else:
                if n<9:
                    return True
                else:
                    if n%3==0:
                        return False
                    else:
                        r=math.floor(math.sqrt(n))  
                        f=5
                        while f<=r:
                            if n%f==0:
                                return False
                                break
                            if n%(f+2)==0:
                                return False
                                break
                            f=f+6
                        return True
                                         
def PrimeFactors(i):
   Factors = list()
   import math
   j = i
   while not isPrime(j):
       for a in range(2, math.floor(math.sqrt(j) + 1)):
           if j%a == 0:
               Factors += (a,)
               j = j/a
               break
   Factors += (j,)
   return list(set(Factors))
   
def totient(n):
   product = 1
   for p in PrimeFactors(n):
       product = product*(1-1/p)
   return n*product

min = 10
value = 1
for i in range(10, 10000001):
   if not isPrime(i):
       a = totient(i)
       if  i/a < min:
           if sorted(str(round(a))) == sorted(str(i)):
               min = i/a
               value = i
   if i%1000000 == 0:
       print(i)

print(value)
       
#The number of coprimes less than a prime n is always n-1, this cannot be a perm.
#My intution was correct, we were looking for two primes around the square root
#of 10**7, I should have stuck with it and came up with a more elegent solution.