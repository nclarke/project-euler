#Problem 213: http://projecteuler.net/problem=213


#Basically the idea is: for each flea find the probabilty of occupancy of each cell on the grid after each bell ring.
#So for example if a flea starts in the centre, after one ring the adjacent cells will each have prob 1/4 of being occupied,
#after two rings each of those probs will be further subdivided and extended out.
#Iterate this over all fleas and all rings and collect the probabilities of occupancy for each cell for each flea.
#Then find the prob of a cell being empty by finding the cumulative product of the prob of each cell not occupied for all fleas (as the probs are
#independent).
#Sum over all cells and you have the expected number of empty cells.


from numpy import *


dim=29

mastergrid = ones((dim+1,dim+1))


for row in range(0,dim+1):
    for column in range(0,dim+1):  #Iterating over the fleas
        grid = zeros((dim+1,dim+1))
        grid2 = zeros((dim+1,dim+1))
        grid[row][column] = 1
        for iteration in range(1,51):   #Iterating over the 50 bell rings
            for i in range(0,dim+1):    #Loop that calculates the change in probability of occupancy for each bell ring.
                for j in range(0,dim+1):
                    if i%dim == 0 or j%dim== 0:
                        if i == 0 and j == 0:
                            grid2[0][1] += (grid[0][0])/2
                            grid2[1][0] += (grid[0][0])/2
                        if i == dim and j == dim:
                            grid2[dim][dim-1] += (grid[dim][dim])/2
                            grid2[dim-1][dim] += (grid[dim][dim])/2
                        if i == 0 and j == dim:
                            grid2[0][dim-1] += (grid[0][dim])/2
                            grid2[1][dim] += (grid[0][dim])/2
                        if i == dim and j == 0:
                            grid2[dim][1] += (grid[dim][0])/2
                            grid2[dim-1][0] += (grid[dim][0])/2
                    #Corner cases
                        if i == 0 and j%dim != 0:
                            grid2[1][j] += (grid[0][j])/3
                            grid2[0][j-1] += (grid[0][j])/3
                            grid2[0][j+1] += (grid[0][j])/3
                        if i == dim and j%dim != 0:
                            grid2[dim-1][j] += (grid[dim][j])/3
                            grid2[dim][j-1] += (grid[dim][j])/3
                            grid2[dim][j+1] += (grid[dim][j])/3
                        if j == 0 and i%dim != 0:
                            grid2[i][1] += (grid[i][0])/3
                            grid2[i+1][0] += (grid[i][0])/3
                            grid2[i-1][0] += (grid[i][0])/3
                        if j == dim and i%dim != 0:
                            grid2[i][dim-1] += (grid[i][dim])/3
                            grid2[i+1][dim] += (grid[i][dim])/3
                            grid2[i-1][dim] += (grid[i][dim])/3
                    #Edge cases

                    else:
                        grid2[i+1][j] += (grid[i][j])/4
                        grid2[i-1][j] += (grid[i][j])/4
                        grid2[i][j+1] += (grid[i][j])/4
                        grid2[i][j-1] += (grid[i][j])/4
                    #Other cases

            grid = grid2
            grid2 = grid2 = zeros((dim+1,dim+1)) #This bit took me ages to figure out. Print each step to see what's going on.
                                                 #Have to reset grid2 after each ring.
        #print(grid)

        #Collecting cumulative prob of each cell being empty
        for a in range(0,dim+1):
            for b in range(0, dim+1):
                mastergrid[a][b] = mastergrid[a][b]*(1-grid[a][b])
        

#Summing the probability of each cell being empty over all cells to obtain total expected number of empty cells.        
sum=0
for bleh in range(0,dim+1):
    for bleh2 in range(0,dim+1):
        sum += mastergrid[bleh][bleh2]


print(sum)

