Hey there.

This is where I will store some selected problems as I work my way through Project Euler.

Problems I really want to solve:

Problem 88: Product-sum numbers 
(http://projecteuler.net/problem=88)

Problem 416: A frog's trip 
(http://projecteuler.net/problem=416)

Problem 301: Nim 
(http://projecteuler.net/problem=301)

Problem 158: Number Mind
(http://projecteuler.net/problem=185)

Problem 243: Resilience
(http://projecteuler.net/problem=243)

Problem 82: Path sum: three ways
(http://projecteuler.net/problem=83)



Project Euler Username: slayerofspartans