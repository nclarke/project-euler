import math

p = list()

#The numerator of the proportion of valid cases (to total number of perms) follows a recursive relationship:
#Numerator(n) = sum from i=0 to i=n ( 2^(i-2) ) + Numerator(n-1)
Initialise
# with numerator of p(2)
Numerator = (1,)	

for n in range(3,27):

	Denominator = 0
	PowerComponent = 0
	
	for i in range(0,n-1):
		PowerComponent += 2**i

	Numerator += (Numerator[-1] + PowerComponent,)

	Denominator = math.factorial(n)

	ValidProportion = Numerator[-1]/float(Denominator)
	
	#This calculates the total number of string permutations of a given length
	perms = 1
	for i in range(0,n):
		perms *= 26-i

	p += (ValidProportion*perms,)

print(max(p))
