#Problem 97:

total = 0
digits = 1

for i in range(0, 7830457):   # The length of the number in base 2 will be one less than the exponent on 2.
    total += digits
    digits = str(digits)
    if len(digits) > 3:
        digits = digits[-12:-1] + digits[-1]  # Should have just use modular division
    digits = 2*int(digits)

print(28433*total + 28433 + 1)

#So what I did here was I first factorised it into a Mersenne prime. Then I used the fact that a Mersenne prime can be expressed
#as a number consisting of ones (in base 2) of length prime exponent - 1.
#So then I just calculated the last ten digits of this by doubling numbers and if they were over say 12 digits long I would chop
#off the first digits. After that just sum and add the rest of the numbers from your factorised equation.
