def isPrime(n):
    import math
    assert n>0
    if n==1:
        return False
    else:
        if n<4:
            return True
        else:
            if  n%2==0:
                return False
            else:
                if n<9:
                    return True
                else:
                    if n%3==0:
                        return False
                    else:
                        r = math.floor(math.sqrt(n))  
                        f=5
                        while f<=r:
                            if n%f==0:
                                return False
                                break
                            if n%(f+2)==0:
                                return False
                                break
                            f=f+6
                        return True
#Fully optimised





# def PrimeFactors(i, primeslist):
#   Factors = list()
#   while not isPrime(i):
#     for prime in primeslist:
#       if i%prime == 0:
#         Factors += (prime,)
#         i = i/prime
#         break
#   Factors += (i,)
#   return list(set(Factors))

#The list of prime factors dividing n
def PrimeFactors(i):
   Factors = list()
   import math
   j = i
   while not isPrime(j):
       for a in range(2, int(math.floor(math.sqrt(j) + 1))):
           if j%a == 0:
               Factors += (a,)
               j = j/a
               break
   Factors += (j,)
   return list(set(Factors))

def totient(n):
   product = 1
   for p in PrimeFactors(n):
       product *= (1-1/p)
   return n*product


# primeslist = list()
# n=2

# Resilience = 1

# while Resilience >= 15499.0/94744:
#   if isPrime(n):
#     primeslist += (n,)
#   Resilience = totient(n+1)/n
#   n += 1
#   print(n)

# print(n-1)



# listofprimes = list()
# for i in range(2,1000):
#   if isPrime(i):
#     listofprimes += (i,)

# print(listofprimes)



# product = 1
# for i in listofprimes[0:10]:
#   product *= i
# print(product)


# print(totient(product)/(product-1))


# print(totient(product/listofprimes[9])/(product/listofprimes[9]-1))

# for i in listofprimes[0:9]:
#   print(i)

print(15499/94744)

print("*1")
test = 2*3*5*7*11*13*17*19*23

print(totient(test)/(test-1))

print("*2")
test = 2*3*5*7*11*13*17*19*23*2

print(totient(test)/(test-1))

print("*3")
test = 2*3*5*7*11*13*17*19*23*3

print(totient(test)/(test-1))

print("*2*3")
test = 2*3*5*7*11*13*17*19*23*2*3

print(totient(test)/(test-1))

print(test)

print(totient(test)/(test-1)<15499/94744)




for i in range(2, 13):
  print(i)
  print(totient(i)/(i-1))


#So, if two is a factor then ever 2nd nunber will not be relatively prime.
#If 3 is a factor every third number will not be relatively prime, etc.















# print(PrimeFactors(684))





# Resilience = 1
# n = 2

# for i in range(2,13):
# 	Resilience = totient(n+1)/n
# 	print(Resilience)
# 	n+=1



#Some notes
#If a number is prime, the Resilience will be one
#Euler's totient function gives the number of integers less than or equal to n that are relatively primes to n.
#So the Resilience is totient(n+1)/n

#The key to this problem I think is to use this fact in conjuntion with some memoization. 